#!/usr/bin/ruby

#$:.unshift File.dirname(__FILE__)
$:.unshift File.join(File.dirname(__FILE__), ".", "..", "lib")
require 'ErrorHandler'
require 'EnvironmentInfo'
require 'yaml'
require 'PathInfo'

# Class to consume a SAM file generated by BWA and apply various fixups to
# produce the final BAM file and calculate alignment stats on it.
# Author: Nirav Shah niravs@bcm.edu. Refactored by Mike D.
#
class DNASeq_BWA_SAMtoBAM
  def initialize(samFileName, finalBAMName, fcBarcode, isFragment)
   
    # Maintain various filenames
    @samFile   = samFileName  # SAM file produced by BWA
    @finalBam  = finalBAMName # Final BAM

    # Intermediate BAM which is coordinate sorted but without marking duplicates
    @sortedBam = samFileName.gsub(/\.sam$/, "_sorted.bam")

    @fcBarcode     = fcBarcode.to_s # Flowcell barcode
    @isFragment    = isFragment     # true if fragment lane, false otherwise

    getEnvironmentConfigParams()
  end

  # Main worker method to produce a BAM
  def makeBAM()
    puts "Displaying execution environment"
    EnvironmentInfo.displayEnvironmentInformation($stdout, true)

    if @isFragment == false
       # For a paired-end BAM, fix the mate information and CIGAR
       cmd = fixMateInfoCustomCommand(@samFile, @sortedBam)
       runCommand(cmd, "MateInfoFixerCustom")
    else
       # For a fragment (single-end) BAM, run SortSAM and then fix CIGAR
      cmd = sortBamCommand(@samFile, @sortedBam)
      runCommand(cmd, "SortBam")

      # Now fix CIGAR
      cmd = fixCIGARCommand(@sortedBam)
      runCommand(cmd, "CigarFixer")
    end

    # Run mark duplicates command
    cmd = markDupCommand(@sortedBam, @finalBam)
    runCommand(cmd, "MarkDuplicates")

    # Calculate mapping and other stats
    cmd = mappingStatsCommand(@finalBam)
    runCommand(cmd, "BAMAnalyzer_MappingStats")
  end

  private

  # Obtain various parameter values to run Picard commands and to run custom
  # Java applications.
  def getEnvironmentConfigParams()
    # Directory hosting various custom-built jars

    @customPicardJarDir  = PathInfo::CUSTOM_PICARD_JARS

    # Read picard parameters from yaml config file
    yamlConfigFile = PathInfo::CONFIG_DIR + "/config_params.yml"
    configReader =  YAML.load_file(yamlConfigFile)
    
    # Parameters for picard commands
    @picardPath       = configReader["picard"]["path"]
    @picardValStr     = configReader["picard"]["stringency"]
    @picardTempDir    = configReader["picard"]["tempDir"]
    @maxRecordsInRam  = configReader["picard"]["maxRecordsInRAM"]
    @heapSize         = configReader["picard"]["maxHeapSize"]
  end

  # Command to sort a BAM
  def sortBamCommand(input, output)
    cmd = "java " + @heapSize + " -jar " + @picardPath + "/SortSam.jar I=" +
           input + " O=" + output + " SO=coordinate " + @picardTempDir +
           " " + @maxRecordsInRam.to_s + " " + @picardValStr  
    return cmd
  end

  # Mark duplicates on a sorted BAM
  def markDupCommand(input, output)
    cmd = "java " + @heapSize + " -jar " + @picardPath + "/MarkDuplicates.jar " +
          " I=" + input +  " O=" + output + " " + @picardTempDir + " " +
          @maxRecordsInRam.to_s + " AS=true M=PCR_Duplicates_metrics.txt " +
          @picardValStr
    return cmd
  end

  # Correct the flag describing the strand of the mate
  # This function uses Picard's FixMateInformation.jar   <--- not used in automated pipeline
  def fixMateInfoPicardCommand(input, output)
    cmd = "java " + @heapSize + " -jar " + @picardPath + "/FixMateInformation.jar " +
          "I=" + input + " O=" + output + " SO=coordinate " + @picardTempDir +
          " " + @maxRecordsInRam.to_s + " " + @picardValStr
    return cmd
  end

  # Use custom tool to fix the flag describing strand of the mate. It will also
  # fix CIGAR for unmapped reads, i.e., include fixCIGARCmd
  def fixMateInfoCustomCommand(input, output)
    jarName = @customPicardJarDir + "/MateInfoFixer.jar"
    cmd = "java " + @heapSize + " -jar " + jarName + " I=" + input +
          " O=" + output + " FUR=true " +  @picardTempDir + " " +
          @maxRecordsInRam.to_s + " " + @picardValStr 
    return cmd
  end

  # Correct the unmapped reads. Reset CIGAR to * and mapping quality to zero.
  # This command overwrites the input file with the fixed file.
  def fixCIGARCommand(input)
    jarName = @customPicardJarDir + "/CIGARFixer.jar"
    cmd = "java " + @heapSize + " -jar " + jarName + " I=" + input
    return cmd
  end

  # Method to build command to calculate mapping stats
  def mappingStatsCommand(input)
    
    puts "\n\nAbout to run BAM Analyzer. Creating DIR BAM_mapping_stats in Sample DIR"
    bamFile = File.expand_path(input)
    Dir.mkdir("BAM_mapping_stats") unless File.exists?("BAM_mapping_stats")
    FileUtils.cd("BAM_mapping_stats")
    jarName = @customPicardJarDir + "/BAMAnalyzer.jar"
    cmd = "java " + @heapSize + " -jar " + jarName + " I=" + bamFile +
          " O=BWA_Map_Stats.txt X=BAMAnalysisInfo.xml"
    return cmd
  end

  # Method to run the specified command
  def runCommand(cmd, cmdName)
    
    updateLog = "\n###################--------- Running command " + cmdName.to_s + " ---------#######################\n"
    $stdout.puts updateLog.to_s
    $stdout.puts "Command syntax is: \n" + cmd + "\n"
    $stderr.puts updateLog.to_s
    startTime = Time.now
    `#{cmd}`
    endTime   = Time.now
    returnValue = $?

    timeDiff = (endTime - startTime) / 3600
    puts "Execution time : " + timeDiff.to_s + " hours"

    if returnValue != 0
      handleError(cmdName)
    else
       $stderr.puts "Process " + cmdName + " ran successfully with exit code " + returnValue.to_s + " \n" 
    end
  end

   # Method to handle error. Current behavior, print the error stage and abort.
  def handleError(commandName)
    $stderr.puts "Process " + commandName.to_s + " failed. Sending email with report. \n"
    #obj            = ErrorMessage.new()
    #obj.msgDetail  = "Error while processing command : " + commandName.to_s
    #obj.msgBrief   = "Error during alignment for flowcell : " + @fcBarcode.to_s
    #obj.fcBarcode  = @fcBarcode.to_s
    #obj.workingDir = Dir.pwd
    #obj.hostName   = EnvironmentInfo.getHostName()
    #ErrorHandler.handleError(obj)
    exit -1
  end
end

param4 = ARGV[3]
if param4.eql?("false")
  isFragment = false
else
  isFragment = true
end

obj = DNASeq_BWA_SAMtoBAM.new(ARGV[0], ARGV[1], ARGV[2], isFragment)
obj.makeBAM()
